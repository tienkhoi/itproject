'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
        'ngRoute',
        'myApp.filters',
        'myApp.services',
        'myApp.directives',
        'myApp.controllers'
    ]).

    //Define routes
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/home', {templateUrl: 'partials/home.html'});
        $routeProvider.when('/guide', {templateUrl: 'partials/guide.html'});
        $routeProvider.when('/estimator', {templateUrl: 'partials/estimator.html', controller: 'MyCtrl1'});
        $routeProvider.when('/project', {templateUrl: 'partials/project.html', controller: 'MyCtrl2'});
        $routeProvider.otherwise({redirectTo: '/home'});
    }]);
