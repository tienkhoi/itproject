'use strict';

/* Controllers */

function HeaderController($scope, $location) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}
angular.module('myApp.controllers', []).
    //Estimator Page Controller
    controller('MyCtrl1', ['$scope', 'inputData', '$http', function ($scope, inputData, $http) {


        $scope.saveBtn = inputData.saveBtn;
        $scope.UpdateBtn = inputData.updateBtn;
        //function Calculate complexity
        $scope.calComplex = function () {

            var totalComplex =

                parseInt($scope.input.f1) +
                    parseInt($scope.input.f2) +
                    parseInt($scope.input.f3) +
                    parseInt($scope.input.f4) +
                    parseInt($scope.input.f5) +
                    parseInt($scope.input.f6) +
                    parseInt($scope.input.f7) +
                    parseInt($scope.input.f8) +
                    parseInt($scope.input.f9) +
                    parseInt($scope.input.f10) +
                    parseInt($scope.input.f11) +
                    parseInt($scope.input.f12) +
                    parseInt($scope.input.f13) +
                    parseInt($scope.input.f14);

            $scope.totalFile = $scope.input.filesWeight * $scope.input.numFile;
            $scope.totalInquiry = $scope.input.inquiriestWeight * $scope.input.numInquriries;
            $scope.totalOutput = $scope.input.outputWeight * $scope.input.numOutput;
            $scope.totalInput = $scope.input.inputWeight * $scope.input.numInput;
            $scope.totalInterfaces = $scope.input.interfaceWeight * $scope.input.numInterfaces;

            $scope.TotalWeightFactors = $scope.totalFile + $scope.totalInterfaces + $scope.totalInquiry + $scope.totalOutput
                + $scope.totalInput
            $scope.result.fp = $scope.TotalWeightFactors * (0.65 + 0.01 * totalComplex);

        }


//Check Preloaded data from project page
        $scope.input = {};
        $scope.result = {};
        $scope.btnSave = "Save"




        if (inputData.data != "") {

            if (inputData.isImport == true) {
                $scope.edit = false;
                $scope.showBtn = false;
                inputData.isEdit = false;
                $scope.input.ProjectName = inputData.data.ProjectName;
                $scope.input.language = inputData.data.language;
                $scope.input.project = inputData.data.project;
                $scope.result.fp = inputData.data.fp;
                $scope.eaf = inputData.data.eaf;
                $scope.result.sloc = inputData.data.sloc;
                $scope.input.salary = inputData.data.salary;
                $scope.input.ProjectName = inputData.data.ProjectName;


            }
            else {
                $scope.input = inputData.data

                $scope.calComplex()

            }


        }

        //EAF formula
        $scope.eaf = $scope.input.reliability * $scope.input.sizeData * $scope.input.complex * $scope.input.performance * $scope.input.memory * $scope.input.environment * $scope.input.times * $scope.input.analyst * $scope.input.applicationExp * $scope.input.capability * $scope.input.virtualMExperience * $scope.input.programExp * $scope.input.enMethod * $scope.input.useSoftware * $scope.input.reqDevSchedle;

        $scope.calSloc = function () {
            $scope.result.sloc = $scope.input.language * $scope.result.fp;

        }

        //
        //Executed when size of group is choosen
        $scope.softwareproject = function (pos) {

            if (pos == 1) {
                console.log(1)
                $scope.input.a = 3.2
                $scope.input.b = 1.05
                $scope.input.c = 2.5
                $scope.input.d = 0.38
            }
            else if (pos == 2) {
                console.log(2)

                $scope.input.a = 3.0
                $scope.input.b = 1.12
                $scope.input.c = 2.5
                $scope.input.d = 0.35
            }
            else if (pos == 3) {
                console.log(3)

                $scope.input.a = 2.8
                $scope.input.b = 1.20
                $scope.input.c = 2.5
                $scope.input.d = 0.32
            }

            if (inputData.isImport == true) {
                $scope.eaf = inputData.data.eaf;
            }
            else {
                $scope.eaf = $scope.input.reliability * $scope.input.sizeData * $scope.input.complex * $scope.input.performance * $scope.input.memory * $scope.input.environment * $scope.input.times * $scope.input.analyst * $scope.input.applicationExp * $scope.input.capability * $scope.input.virtualMExperience * $scope.input.programExp * $scope.input.enMethod * $scope.input.useSoftware * $scope.input.reqDevSchedle;

            }


            console.log($scope.eaf)
            $scope.result.effort = $scope.input.a * Math.pow(Number($scope.result.sloc) / 1000, $scope.input.b) * $scope.eaf;
            console.log($scope.result.effort

            )
            $scope.result.duration = $scope.input.c * Math.pow($scope.result.effort, $scope.input.d)
            console.log($scope.result.duration)


        }


        //Function update current Project
        $scope.update = function () {
            var record = {
                'ProjectName': $scope.input.ProjectName,
                'numInput': $scope.input.numInput,
                'numOutput': $scope.input.numOutput,
                'numInquriries': $scope.input.numInquriries,
                'numFile': $scope.input.numFile,
                'numInterfaces': $scope.input.numInterfaces,
                'inputWeight': $scope.input.inputWeight,
                'outputWeight': $scope.input.outputWeight,
                'inquiriestWeight': $scope.input.inquiriestWeight,
                'filesWeight': $scope.input.filesWeight,
                'interfaceWeight': $scope.input.interfaceWeight,
                'f1': $scope.input.f1,
                'f2': $scope.input.f2,
                'f3': $scope.input.f3,
                'f4': $scope.input.f4,
                'f5': $scope.input.f5,
                'f6': $scope.input.f6,
                'f7': $scope.input.f7,
                'f8': $scope.input.f8,
                'f9': $scope.input.f9,
                'f10': $scope.input.f10,
                'f11': $scope.input.f11,
                'f12': $scope.input.f12,
                'f13': $scope.input.f13,
                'f14': $scope.input.f14,
                'language': $scope.input.language,
                'reliability': $scope.input.reliability,
                'sizeData': $scope.input.sizeData,
                'complex': $scope.input.complex,
                'performance': $scope.input.performance,
                'memory': $scope.input.memory,
                'environment': $scope.input.environment,
                'times': $scope.input.times,
                'analyst': $scope.input.analyst,
                'applicationExp': $scope.input.applicationExp,
                'capability': $scope.input.capability,
                'virtualMExperience': $scope.input.virtualMExperience,
                'programExp': $scope.input.programExp,
                'enMethod': $scope.input.enMethod,
                'useSoftware': $scope.input.useSoftware,
                'reqDevSchedle': $scope.input.reqDevSchedle,
                'teama': $scope.input.a,
                'teamb': $scope.input.b,
                'teamc': $scope.input.c,
                'teamd': $scope.input.d,
                'project': $scope.input.project,
                'salary': $scope.input.salary


            }

            //Call Web API to edit
            $http.put('http://start.khoivan.com/public/api/project/' + inputData.data.id, {data: record})
                .success(function (data) {
                    alert("Project has been updated")
                })
                .error(function (data) {
                    alert('Error occured')
                });


        }

        //Function saving new project
        $scope.save = function () {
            var record = {
                'ProjectName': $scope.input.ProjectName,
                'numInput': $scope.input.numInput,
                'numOutput': $scope.input.numOutput,
                'numInquriries': $scope.input.numInquriries,
                'numFile': $scope.input.numFile,
                'numInterfaces': $scope.input.numInterfaces,
                'inputWeight': $scope.input.inputWeight,
                'outputWeight': $scope.input.outputWeight,
                'inquiriestWeight': $scope.input.inquiriestWeight,
                'filesWeight': $scope.input.filesWeight,
                'interfaceWeight': $scope.input.interfaceWeight,
                'f1': $scope.input.f1,
                'f2': $scope.input.f2,
                'f3': $scope.input.f3,
                'f4': $scope.input.f4,
                'f5': $scope.input.f5,
                'f6': $scope.input.f6,
                'f7': $scope.input.f7,
                'f8': $scope.input.f8,
                'f9': $scope.input.f9,
                'f10': $scope.input.f10,
                'f11': $scope.input.f11,
                'f12': $scope.input.f12,
                'f13': $scope.input.f13,
                'f14': $scope.input.f14,
                'language': $scope.input.language,
                'reliability': $scope.input.reliability,
                'sizeData': $scope.input.sizeData,
                'complex': $scope.input.complex,
                'performance': $scope.input.performance,
                'memory': $scope.input.memory,
                'environment': $scope.input.environment,
                'times': $scope.input.times,
                'analyst': $scope.input.analyst,
                'applicationExp': $scope.input.applicationExp,
                'capability': $scope.input.capability,
                'virtualMExperience': $scope.input.virtualMExperience,
                'programExp': $scope.input.programExp,
                'enMethod': $scope.input.enMethod,
                'useSoftware': $scope.input.useSoftware,
                'reqDevSchedle': $scope.input.reqDevSchedle,
                'teama': $scope.input.a,
                'teamb': $scope.input.b,
                'teamc': $scope.input.c,
                'teamd': $scope.input.d,
                'project': $scope.input.project,
                'salary': $scope.input.salary


            }

            //Call webapi to create new one
            $http.post('http://start.khoivan.com/public/api/project', {data: record})
                .success(function (data) {
                    alert("Project has been saved")
                })
                .error(function (data) {
                    alert('Error occured')
                });


        }


    }])
    //Project Page Controller
    .controller('MyCtrl2', ['$scope', '$http', '$location', 'inputData', function ($scope, $http, $location, inputData) {



        //Parse file when csv is uploaded
        $('#submit').click(function () {
            $('table').show()
            inputData.saveBtn = false;
            $('#file').parse({
                config: {
//
                },
                //This happens when csv is loaded completely
                complete: function (results, file, inputElem, event) {

                    $.each(results.results.rows, function (index, value) {
                        value.salary = value.CostPerPM;
                        delete value.CostPerPM;
                        value.eaf = value.EAF;
                        delete value.EAF;
                        value.fp = value.FunctionPoints;
                        delete value.FunctionPoints;

                        switch (value.ProjectClass) {
                            case 'Semi-Detached':
                                value.project = 2;
                                break;
                            case 'Organic':
                                value.project = 1;
                                break;
                            case 'Embedded':
                                value.project = 3;
                                break;
                        }
                        delete value.ProjectClass;

                        value.sloc = value.LoC;
                        delete value.LoC;


                        switch (value.Language) {
                            case "C":
                                value.language = 128;
                                break;
                            case "C++":
                                value.language = 55;

                                break;

                            case "Java":
                                value.language = 53;
                                break;
                            case "Fourth Generation Language":
                                value.language = 20;

                                break;


                            case "Fifth Generation Language":

                                value.language = 4;
                            case "Assembly - Basic":

                                value.language = 320;

                                break;

                            case "COBO":
                                value.language = 91;

                                break;
                            default :
                                value.language = 50;
                                break;
                        }


                        delete value.Language;


                    });
//Imports contains array of project loaded from csv
                    $scope.$apply(function () {
                        $scope.imports = results.results.rows;
                        inputData.isImport = true;

                    })


                }
            });
        });


        //Get all project from database
        $http.get('http://start.khoivan.com/public/api/project')
            .success(function (data) {
                console.log(data)
                $scope.projects = data.projects;

            })
            .error(function (data) {
            });


        //Change view with preloaded data
        $scope.changeView = function (project, view,save,update) {
            if(save==1)
            {
                inputData.saveBtn = true;
            }
            else
            {
                inputData.saveBtn = false
            }

            if(update==1)
            {
                inputData.updateBtn = true;
            }
            else
            {
                inputData.updateBtn = false
            }
            inputData.isEdit = true;
            inputData.data = project;
            console.log(project)
            $location.url(view); // path not hash
        }

        //Generate PDF using DOMPDF library

        $scope.generatePDF = function (project) {
            var link = "../backend/storage.php?act=generatePDF";

            console.log(project)

            $.each(project, function (index, element) {
                link += "&" + index + "=" + element;

            });
            window.location.assign(link)


        }
        //Call API to delete
        $scope.delete = function (project) {

            $http.delete('http://start.khoivan.com/public/api/project/' + project.id)
                .success(function (data) {
                    alert("Project has been deleted")

                    $http.get('http://start.khoivan.com/public/api/project')
                        .success(function (data) {
                            console.log(data)
                            $scope.projects = data.projects;

                        })
                        .error(function (data) {
                        });

                })
                .error(function (data) {
                    alert('Error occured')
                });


        }

    }]);