<?php
/**
 * Created by PhpStorm.
 * User: KVan
 * Date: 2/11/14
 * Time: 10:04 AM
 */
switch ($_REQUEST['act']) {

    //function use to generate PDF
    case "generatePDF":
        require_once("/dompdf/dompdf_config.inc.php");


        $totalFile = $_GET['filesWeight'] * $_GET['numFile'];
        $totalInquiry = $_GET['inquiriestWeight'] * $_GET['numInquriries'];
        $totalOutput = $_GET['outputWeight'] * $_GET['numOutput'];
        $totalInput = $_GET['inputWeight'] * $_GET['numInput'];
        $totalInterfaces = $_GET['interfaceWeight'] * $_GET['numInterfaces'];

        $totalComplex = $_GET['f1'] + $_GET['f2'] + $_GET['f3'] + $_GET['f4'] + $_GET['f5'] + $_GET['f6'] + $_GET['f7'] +
            $_GET['f8'] + $_GET['f9'] + $_GET['f10'] + $_GET['f11'] + $_GET['f12'] + $_GET['f13'] + $_GET['f14'];

        $TotalWeightFactors = $totalFile + $totalInterfaces + $totalInquiry + $totalOutput
            + $totalInput;

        switch ($_GET['language']) {
            case 128:
                $language = "C";
                break;
            case 55:
                $language = "C++";
                break;

            case 53:
                $language = "Java";
                break;
            case 20:
                $language = "Fourth Generation Language";
                break;
            case 29:
                $language = "Visual Basic 5.0";
                break;

            case 4:
                $language = "Fifth Generation Language";
                break;
            case 320:
                $language = "Assembly - Basic";
                break;

            case 91:
                $language = "Cobo";
                break;


        }

        $fp = $TotalWeightFactors * (0.65 + 0.01 * $totalComplex);
        $sloc = $_GET['language'] * $fp;
        $eaf = $_GET['reliability'] * $_GET['sizeData'] * $_GET['complex'] * $_GET['performance'] * $_GET['memory'] *
            $_GET['environment'] * $_GET['times'] * $_GET['analyst'] * $_GET['applicationExp'] * $_GET['capability'] * $_GET['virtualMExperience'] *
            $_GET['programExp'] * $_GET['enMethod'] * $_GET['useSoftware'] * $_GET['reqDevSchedle'];


        $effort = $_GET['teama'] * pow(($sloc) / 1000, $_GET['teamb']) * $eaf;
        $duration = $_GET['teamc'] * pow($effort, $_GET['teamd']);

        $cost = $effort * $_GET['salary'];
        $doc = $sloc / 24;
        $error = $sloc / 84;
        $people = $effort / $duration;


        var_dump($duration);
        $html = "<h1 style='text-align: center'><b>BLUENIX</b></h2>";

        $html = "<h2 style='text-align: center'>Project Estimator Report</h2>";
        $html .= "<h2 style='text-align: center'>Project title : " . $_REQUEST['ProjectName'] . "</h2>";


        $html .= "<br/>";
        $html .= "<br/>";
        $html .= "<hr/>";


        $html .= "<table style='margin-left:50px;border-collapse: 1px;width: 300px'>";

        $html .= "<tr>";
        $html .= "<td>Function Points:";
        $html .= "</td>";
        $html .= "<td>" . $fp;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Language:";
        $html .= "</td>";
        $html .= "<td>" . $language;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Lines of code:";
        $html .= "</td>";
        $html .= "<td>" . $sloc;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Effort:";
        $html .= "</td>";
        $html .= "<td>" . (int)$effort;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Duration:";
        $html .= "</td>";
        $html .= "<td>" . (int)$duration;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Cost:";
        $html .= "</td>";
        $html .= "<td>" . (int)$cost;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Pages of documentation:";
        $html .= "</td>";
        $html .= "<td>" . (int)$doc;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Errors:";
        $html .= "</td>";
        $html .= "<td>" . (int)$error;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Number of developers:";
        $html .= "</td>";
        $html .= "<td>" . (int)$people;
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Average Salary:";
        $html .= "</td>";
        $html .= "<td>" . $_GET['salary'] . "$";
        $html .= "</td>";
        $html .= "</tr>";

        $html .= "</table>";
        $html .= "<hr/>";

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper("letter");
        $dompdf->render();
        $canvas = $dompdf->get_canvas();
        $canvas->page_text(290, 750, "Page: {PAGE_NUM} of {PAGE_COUNT}", $font, 7, array(0, 0, 0));

        //group the pdf output
        $output = $dompdf->output();

        $dompdf->stream('report-' . $_GET['ProjectName'] . date("Ymd") . '.pdf');


        break;
}
?>